// SWIPER JS
const testimonialThumbs = new Swiper(".testimonial__thumbs", {
  spaceBetween: 10,
  slidesPerView: 3,
  centeredSlides: true,
  freeMode: true,
  autoplay: true,
  loop: true,
  speed: 2000,
});

const testimonialContent = new Swiper(".testimonial__text", {
  spaceBetween: 10,
  autoplay: true,
  loop: true,
  speed: 2000,
  centeredSlides: true,
  thumbs: {
    swiper: testimonialThumbs,
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
});

// OWL CAROUSEL
$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 30,
  });
});

// WOWJS
new WOW().init();

// COUNTUP JS
$(".counter").countUp();

// LIGHTGALLERY
lightGallery(document.getElementById("gallery"), {
  selector: ".portfolio__overlay-icon",
  plugins: [lgZoom, lgThumbnail, lgAutoplay, lgFullscreen, lgShare],
  loop: true,
  allowMediaOverlap: true,
  toggleThumb: true,
  showZoomInOutIcons: true,
  actualSize: false,
  exThumbImage: "data-exthumbimage",
});
